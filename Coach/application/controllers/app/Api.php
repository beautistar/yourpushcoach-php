<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {     

    function __construct(){

        parent::__construct();
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('app/api_model', 'api_model');
        $this->load->library('session');     
    }
    
    /**
     * Make json response to the client with result code message
     *
     * @param p_result_code : Result code
     * @param p_result_msg : Result message
     * @param p_result : Result json object
     */

    private function doRespond($p_result_code,  $p_result){

         $p_result['result_code'] = $p_result_code;

         $this->output->set_content_type('application/json')->set_output(json_encode($p_result));
    }

    /**
     * Make json response to the client with success.
     * (result_code = 0, result_msg = "success")
     *
     * @param p_result : Result json object
     */

    private function doRespondSuccess($result){

        $result['message'] = "Success.";
        $this->doRespond(200, $result);
    }

    /**
     * Url decode.
     *
     * @param p_text : Data to decode
     *
     * @return text : Decoded text
     */

    private function doUrlDecode($p_text){

        $p_text = urldecode($p_text);
        $p_text = str_replace('&#40;', '(', $p_text);
        $p_text = str_replace('&#41;', ')', $p_text);
        $p_text = str_replace('%40', '@', $p_text);
        $p_text = str_replace('%20',' ', $p_text);
        $p_text = trim($p_text);


        return $p_text;
    }
    
    function version() {
        phpinfo();
    }    
    
    /**
    * get user object from query array result
    * 
    * @param mixed $user_array query result array
    */
    function getUser($user_array) {
        
        $user_object = array('user_id' => $user_array['id'],                             
                             'name' => $user_array['name'],
                             'email' => $user_array['email'],
                             'user_type' => $user_array['user_type'],
                             'photo_url' => $user_array['photo_url'],
                             'created_at' => $user_array['created_at']
                             );
        return $user_object;
    }
    
    function getDocumentOne($row) {
        
        $object = array('id' => $row['id'],                             
                     'name' => $row['name'], 
                     'file_url' => $row['file_url'],
                     'created_at' => $row['created_at']
                             );
        return $object;
        
    }    

    function signup() {
        
        $result = array();
        
        $name = $_POST['name']; 
        $user_type = $_POST['user_type'];
        $email = $_POST['email'];
        $password = $_POST['password'];
        
        if ($this->api_model->existEmail($email)) {
             $result['message'] = "Email already exist.";
             $this->doRespond(201, $result);             
        } else {
            
            
            if(!is_dir("uploadfiles/")) {
                mkdir("uploadfiles/");
            }
            $upload_path = "uploadfiles/";  

            $cur_time = time();
             
            $dateY = date("Y", $cur_time);
            $dateM = date("m", $cur_time);
             
            if(!is_dir($upload_path."/".$dateY)){
                mkdir($upload_path."/".$dateY);
            }
            if(!is_dir($upload_path."/".$dateY."/".$dateM)){
                mkdir($upload_path."/".$dateY."/".$dateM);
            }
             
            $upload_path .= $dateY."/".$dateM."/";
            $upload_url = base_url().$upload_path;

            // Upload file. 

            $w_uploadConfig = array(
                'upload_path' => $upload_path,
                'upload_url' => $upload_url,
                'allowed_types' => "*",
                'overwrite' => TRUE,
                'max_size' => "100000KB",
                'max_width' => 3000,
                'max_height' => 3000,
                'file_name' => $dateY.$dateM.intval(microtime(true) * 10)
            );

            $this->load->library('upload', $w_uploadConfig);

            if ($this->upload->do_upload('photo')) {

                $file_url = $w_uploadConfig['upload_url'].$this->upload->file_name;
                $data = array('name' => $name,
                          'email' => $email,
                          'user_type' => $user_type,
                          'photo_url' => $file_url,
                          'password' => password_hash($password, PASSWORD_BCRYPT),
                          'created_at' => date('Y-m-d h:m:s'),                           
                          );
                $id = $this->api_model->addUser($data);
                $result['image_url'] = $file_url;            
                $result['id'] = $id;            
                $this->doRespondSuccess($result);

            } else {

                $this->doRespond(201, $result);// upload fail
                return;
            }
             
        }
    }
    
    function updateProfile() {
        
        $result = array();
        
        $id = $this->input->post('user_id');
        $name= $this->input->post('name');
        $email = $this->input->post('email');
        
        if ($this->api_model->existEmailWithId($id, $email)) {
             $result['message'] = "Email already exist.";
             $this->doRespond(201, $result);             
        } else {            

            $data = array('name' => $name,
                      'email' => $email,                                                 
                      );
            $this->api_model->updateProfile($id, $data);
            $q_result = $this->api_model->getProfile($id);        
            $result['user_model'] = $this->getUser($q_result);
            $this->doRespondSuccess($result); 
        }
    }    
    
    function signin() {
        
        $result = array();
        
        $email = $_POST['email'];
        $password = $_POST['password'];
            
        $data = array(
            'email' => $email,
            'password' => $password
        );
        $q_result = $this->api_model->login($data);
        if ($q_result == TRUE) {
            $result['user_model'] = $this->getUser($q_result);
            $this->doRespondSuccess($result);            
        } else{
            $result['message'] = 'Invalid Email or Password!';
            $this->doRespond(202, $result);
        }        
    }
    
    function addEvent() {
        
        $result = array();
        
        $name = $this->input->post('name');
        $date = $this->input->post('date');
        $description = $this->input->post('description'); 
        
        $data = array('name' => $name,
                      'description' => $description,                          
                      'date' => $date, 
                      'created_at' => date('Y-m-d h:m:s')
                      );
        $result['id'] = $this->api_model->addEvent($data);          
        $this->doRespondSuccess($result); 
    }
    
    function getEvent() {
        
        $result = array();
        $result['event_list'] = $this->api_model->getEvent();
        $this->doRespondSuccess($result);
    }
    
    function addPackage() {
        
        $result = array();
        
        $name = $this->input->post('name');
        $description = $this->input->post('description'); 

        $data = array('name' => $name,
                      'description' => $description,                          
                      'created_at' => date('Y-m-d h:m:s')
                      );
        $result['id'] = $this->api_model->addPackage($data);
        $this->doRespondSuccess($result);  
    }
    
    function getPackage() {
        
        $result = array();
        $result['package_list'] = $this->api_model->getPackage();
        $this->doRespondSuccess($result);
    }
    
    function uploadPhoto() {
         
         $result = array();
         
         $user_id = $_POST['user_id'];

                  
         if(!is_dir("uploadfiles/")) {
             mkdir("uploadfiles/");
         }
         $upload_path = "uploadfiles/";  

         $cur_time = time();
         
         $dateY = date("Y", $cur_time);
         $dateM = date("m", $cur_time);
         
         if(!is_dir($upload_path."/".$dateY)){
             mkdir($upload_path."/".$dateY);
         }
         if(!is_dir($upload_path."/".$dateY."/".$dateM)){
             mkdir($upload_path."/".$dateY."/".$dateM);
         }
         
         $upload_path .= $dateY."/".$dateM."/";
         $upload_url = base_url().$upload_path;

        // Upload file. 

        $w_uploadConfig = array(
            'upload_path' => $upload_path,
            'upload_url' => $upload_url,
            'allowed_types' => "*",
            'overwrite' => TRUE,
            'max_size' => "100000KB",
            'max_width' => 3000,
            'max_height' => 4000,
            'file_name' => $user_id.'_'.intval(microtime(true) * 10)
        );

        $this->load->library('upload', $w_uploadConfig);

        if ($this->upload->do_upload('photo')) {

            $file_url = $w_uploadConfig['upload_url'].$this->upload->file_name;

            $id = $this->api_model->uploadPhoto($user_id, $file_url);
            $result['photo_url'] = $file_url;            
            $this->doRespondSuccess($result);

        } else {

            $this->doRespond(206, $result);// upload fail
            return;
        }     
    }
    
    function createDocument() {
         
         $result = array();
         
         $user_id = $_POST['user_id'];
         $name = $_POST['name'];

                  
         if(!is_dir("uploadfiles/")) {
             mkdir("uploadfiles/");
         }
         $upload_path = "uploadfiles/";  

         $cur_time = time();
         
         $dateY = date("Y", $cur_time);
         $dateM = date("m", $cur_time);
         
         if(!is_dir($upload_path."/".$dateY)){
             mkdir($upload_path."/".$dateY);
         }
         if(!is_dir($upload_path."/".$dateY."/".$dateM)){
             mkdir($upload_path."/".$dateY."/".$dateM);
         }
         
         $upload_path .= $dateY."/".$dateM."/";
         $upload_url = base_url().$upload_path;

        // Upload file. 

        $w_uploadConfig = array(
            'upload_path' => $upload_path,
            'upload_url' => $upload_url,
            'allowed_types' => "*",
            'overwrite' => TRUE,
            'max_size' => "100000KB",
            'max_width' => 3000,
            'max_height' => 4000,
            'file_name' => $user_id.'_'.intval(microtime(true) * 10)
        );

        $this->load->library('upload', $w_uploadConfig);

        if ($this->upload->do_upload('file')) {

            $file_url = $w_uploadConfig['upload_url'].$this->upload->file_name;
            $data = array('user_id' => $user_id,
                          'name' => $name,
                          'file_url' => $file_url,
                          'created_at' => date('Y-m-d h:m:s'));

            $id = $this->api_model->createDocument($data);
            $result['id'] = $id;            
            $result['file_url'] = $file_url;            
            $this->doRespondSuccess($result);

        } else {

            $this->doRespond(206, $result);// upload fail
            return;
        }     
    }
    
    function getDocument() {
        
        $result = array();
        $t_result = array();
        $user_id = $this->input->post('user_id');
        $q_result = $this->api_model->getDocument($user_id);
        
        foreach ($q_result as $row) {
            array_push($t_result, $this->getDocumentOne($row));
        }
        
        $result['document_list'] = $t_result;
        $this->doRespondSuccess($result);
    }
    
    function createCourse() {
        
        $result = array();
        $urls = array();
         
        $user_id = $this->input->post('user_id');
        $name = $this->input->post('name');
        $data = array('name' => $name,
                      'user_id' => $user_id,
                      'created_at' => date('Y-m-d h:m:s'));          
         
        $course_id = $this->api_model->createCourse($data);
        
        if(isset($_FILES['file']) /*is_uploaded_file($_FILES['file']['tmp_name'][0])*/) {
            
            if(!is_dir("uploadfiles/")) {
                 mkdir("uploadfiles/");
            }
            $upload_path = "uploadfiles/";  

            $cur_time = time();
             
            $dateY = date("Y", $cur_time);
            $dateM = date("m", $cur_time);
             
            if(!is_dir($upload_path."/".$dateY)){
                mkdir($upload_path."/".$dateY);
            }
            if(!is_dir($upload_path."/".$dateY."/".$dateM)){
                mkdir($upload_path."/".$dateY."/".$dateM);
            }
             
            $upload_path .= $dateY."/".$dateM."/";
            $upload_url = base_url().$upload_path;
             
            // Upload file.

            for ($i = 0 ; $i < count($_FILES['file']['name']) ; $i++) {
             
                $filename = $_FILES['file']['name'][$i];
                
                $tmpFilePath = $_FILES['file']['tmp_name'][$i];
                $arrName =  explode('.', $filename);
                $ext = end($arrName);
                $uploadFileName = $i.intval(microtime(true) * 10).'.'.$ext;

                move_uploaded_file($tmpFilePath, $upload_path.$uploadFileName);
                
                $file_url = $upload_url . $uploadFileName;            
                $data = array('course_id' => $course_id, 'file_url' => $file_url); 
                $this->api_model->createCourseFile($data);
                $urls[$i] = $file_url;                                            
            }
        }
        $result['id'] = $course_id;
        $result['files'] = $urls; 
        $this->doRespondSuccess($result);
    }
    
    // get course by course creator
    function getCourse() {
        
        $result = array();
        
        $user_id = $this->input->post('user_id');
        
        $result['course_list'] = $this->api_model->getCourse($user_id);
        
        $this->doRespondSuccess($result);
    }
    
    function addOptionToPackage() {
        
        $result = array();
        $name = $this->input->post('name');
        $package_id = $this->input->post('package_id');
        
        $data = array('package_id' => $package_id,
                      'name' => $name,
                      'created_at' => date('Y-m-d h:m:s'));
        $result['id'] = $this->api_model->addOptionToPackage($data);
        $this->doRespondSuccess($result);
    }
    
    function getProfile() {
        
        $result = array();
        
        $user_id = $_POST['user_id'];             
        
        $q_result = $this->api_model->getProfile($user_id);
        
        $result['user_model'] = $this->getUser($q_result);
        $this->doRespondSuccess($result);            
               
    }

    //admin
    function editEvent() {
        
        $result = array();
        
        $id = $this->input->post('id');
        $name = $this->input->post('name');
        $date = $this->input->post('date');
        $description = $this->input->post('description'); 
        
        $data = array('name' => $name,
                      'description' => $description,                          
                      'date' => $date, 
                      'created_at' => date('Y-m-d h:m:s')
                      );
        $this->api_model->editEvent($id, $data);          
        $this->doRespondSuccess($result);        
        
    }
    
    function deleteEvent() {
        
        $result = array();
        
        $id = $this->input->post('id');         
        
        $this->api_model->deleteEvent($id);          
        $this->doRespondSuccess($result);       
        
    }
    
    function getUpcomingEvent() {
        
        $result = array();
        
        $result['event_list'] = $this->api_model->upcomingEvent();
        $this->doRespondSuccess($result);         
    }
    
    function editPackage() {
        
        $result = array();
        
        $id = $this->input->post('id');
        $name = $this->input->post('name');
        $description = $this->input->post('description'); 

        $data = array('name' => $name,
                      'description' => $description,
                      );
        $this->api_model->editPackage($id, $data);
        $this->doRespondSuccess($result);   
        
    }
    
    function deleteOption() {
        
        $result = array();
        
        $id = $this->input->post('id');         
        
        $this->api_model->deleteOption($id);          
        $this->doRespondSuccess($result);
        
    }
    
    function getUserList() {
        
        $result = array();
        $t_result = array();
        
        $q_result = $this->api_model->getUserList();
        
        foreach ($q_result as $user) {
            $one = $this->getUser($user);
            array_push($t_result, $one);
        }
        
        $result['user_list'] = $t_result;
        $this->doRespondSuccess($result);
    }
    
    function getCoachList() {
        
        $result = array();
        $t_result = array();
        
        $q_result = $this->api_model->getCoachList();
        
        foreach ($q_result as $user) {
            $one = $this->getUser($user);
            array_push($t_result, $one);
        }
        
        $result['coach_list'] = $t_result;
        $this->doRespondSuccess($result);
    }
    
    function deleteUser() {
        
        $result = array();
        
        $id = $this->input->post('id');         
        
        $this->api_model->deleteUser($id);          
        $this->doRespondSuccess($result);
    }
    
    function deletePackage() {
        
        $result = array();
        
        $id = $this->input->post('id');         
        
        $this->api_model->deletePackage($id);          
        $this->doRespondSuccess($result);
        
        
    }
    
    function deleteCourse() {
        
        $result = array();
        
        $id = $this->input->post('id');         
        
        $this->api_model->deleteCourse($id);          
        $this->doRespondSuccess($result);         
        
    }
    
    function addUserToCoach() {
        
        $result = array();
        $coach_id = $this->input->post('coach_id');
        $user_id = $this->input->post('user_id'); 
        $this->api_model->addUserToCoach($coach_id, $user_id);
        $this->doRespondSuccess($result);
    }
    
    function getUserListInCoach() {
        
        $result = array();
        $t_result = array();
        
        $coach_id = $this->input->post('coach_id');
        
        $q_result = $this->api_model->getUserListInCoach($coach_id);
        foreach ($q_result as $user) {
            $one = $this->getUser($user);
            array_push($t_result, $one);
        }
        
        $result['user_list'] = $t_result;
        $this->doRespondSuccess($result);        
    }
    
    function selectPackageOption() {
        
        $result = array();
        $user_id = $this->input->post('user_id');
        $package_id = $this->input->post('package_id');
        $option_id = $this->input->post('option_id');
        
        $data = array('user_id' => $user_id,
                      'package_id' => $package_id,
                      'option_id' => $option_id);
        
        if ($this->api_model->selectPackageOption($data)) {
            $this->doRespondSuccess($result);
        } else {
            $result['message'] = 'User already selected this package option';
            $this->doRespond(202, $result);
        }
    }
    
    function editCourse() {
        
        $result = array();
        $urls = array();
         
        $course_id = $this->input->post('id');
        $user_id = $this->input->post('user_id');
        $name = $this->input->post('name');
        $data = array('name' => $name,
                      'user_id' => $user_id,
                      'created_at' => date('Y-m-d h:m:s'));          
         
        $this->api_model->editCourse($course_id, $data);
        
        if(isset($_FILES['file']) /*is_uploaded_file($_FILES['file']['tmp_name'][0])*/) {
            
            if(!is_dir("uploadfiles/")) {
                 mkdir("uploadfiles/");
            }
            $upload_path = "uploadfiles/";  

            $cur_time = time();
             
            $dateY = date("Y", $cur_time);
            $dateM = date("m", $cur_time);
             
            if(!is_dir($upload_path."/".$dateY)){
                mkdir($upload_path."/".$dateY);
            }
            if(!is_dir($upload_path."/".$dateY."/".$dateM)){
                mkdir($upload_path."/".$dateY."/".$dateM);
            }
             
            $upload_path .= $dateY."/".$dateM."/";
            $upload_url = base_url().$upload_path;
             
            // Upload file.

            for ($i = 0 ; $i < count($_FILES['file']['name']) ; $i++) {
             
                $filename = $_FILES['file']['name'][$i];
                
                $tmpFilePath = $_FILES['file']['tmp_name'][$i];
                $arrName =  explode('.', $filename);
                $ext = end($arrName);
                $uploadFileName = $i.intval(microtime(true) * 10).'.'.$ext;

                move_uploaded_file($tmpFilePath, $upload_path.$uploadFileName);
                
                $file_url = $upload_url . $uploadFileName;            
                $data = array('course_id' => $course_id, 'file_url' => $file_url); 
                $this->api_model->createCourseFile($data);
                $urls[$i] = $file_url;                                            
            }
        }
        $result['id'] = $course_id;
        $result['files'] = $urls; 
        $this->doRespondSuccess($result);
    }
    
    function deleteCourseFile() {
        
        $result = array();
        $course_id = $this->input->post('course_id');
        $file_url = $this->input->post('file_url');
        
        $data = array('course_id' => $course_id,
                      'file_url' => $file_url);
        $this->api_model->deleteCourseFile($data);
        $this->doRespondSuccess($result);
    }
    
    function getCourseForUser() {
        
        $result = array();
        $user_id = $this->input->post('user_id');
        
        $result['course_list'] = $this->api_model->getCourseForUser($user_id);
        
        $this->doRespondSuccess($result);
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
        
    
    
                                     
    
}
?>
