<?php
  
  class Api_model extends CI_Model {
      
      function existEmail($email) {
          
          $query = $this->db->get_where('tb_user', array('email' => $email));
          if ($query->num_rows() == 0){
                return false;
          } else {
                return true;
          }
      }
      
      function existAdminWithUserType($email, $type) {
          
          $query = $this->db->get_where('tb_user', array('email' => $email, 'user_type' => $type));
          if ($query->num_rows() == 0){
                return false;
          } else {
                return true;
          }
      }
      
      function existEmailWithId($id, $email) {
          
          $query = $this->db->get_where('tb_user', array('email' => $email, 'id !=', $id));
          if ($query->num_rows() == 0){
                return false;
          } else {
                return true;
          }
      }
      
      function addUser($data) {
          
          $this->db->insert('tb_user', $data);
          return $this->db->insert_id();
      }
      
      public function login($data){
            $query = $this->db->get_where('tb_user', array('email' => $data['email']));
            if ($query->num_rows() == 0){
                return false;
            }
            else{
                //Compare the password attempt with the password we have stored.
                $result = $query->row_array();
                $validPassword = password_verify($data['password'], $result['password']);
                if($validPassword){
                    return $result = $query->row_array();
                }                
            }
      }
      
      function addEvent($data) {
          
          $this->db->insert('tb_event', $data);
          return $this->db->insert_id();           
      }
      
      function getEvent() {
          
          return $this->db->get('tb_event')->result_array();
      }
      
      function addPackage($data) {
          
          $this->db->insert('tb_package', $data);
          return $this->db->insert_id();           
      }
      
      function getPackage() {
          
          $result = array();
          
          $packages = $this->db->get('tb_package')->result_array();
          
          foreach($packages as $package) {
              $options = array();
              $options = $this->db->get_where('tb_option', array('package_id' => $package['id']))->result_array();
              $package['options'] = $options;  
              array_push($result, $package);
          }
          
          return $result;
      }
      
      function updateProfile($id, $data) {
          
          $this->db->where('id', $id);
          $this->db->update('tb_user', $data);
      }
      
      function uploadPhoto($user_id, $file_url) {
          
          $this->db->where('id', $user_id);
          $this->db->set('photo_url', $file_url);
          $this->db->update('tb_user');
          
      }
      
      function createDocument($data) {
          
          $this->db->insert('tb_document', $data);
      }
      
      function getDocument($user_id) {
          $this->db->where('user_id', $user_id);
          return $this->db->get('tb_document')->result_array();
      }
      
      function createCourse($data) {
          $this->db->insert('tb_course', $data);
          return $this->db->insert_id();
      }
      
      function createCourseFile($data) {
          
          $this->db->insert('tb_course_file', $data);
      }
      
      function getCourse($user_id) {
          
          $result = array();
          
          $course_list = $this->db->get_where('tb_course', array('user_id' => $user_id))
                    ->result_array();
                    
          foreach ($course_list as $course) {
              $files_query = $this->db->get_where('tb_course_file', 
                                                  array('course_id' => $course['id']));
              $files = array();
              if ($files_query->num_rows() > 0) {
                  
                  foreach ($files_query->result_array() as $row)
                  array_push($files, $row['file_url']);                  
              } 
              
              $course['file_url'] = $files;
              array_push($result, $course);
          }
          
          return $result;          
      }
      
      function addOptionToPackage($data) {
          
          $this->db->insert('tb_option', $data);
          return $this->db->insert_id();          
          
      }
      
      function getProfile($user_id) {
          
          return $this->db->get_where('tb_user', array('id' => $user_id))->row_array();
      }
      
      function editEvent($id, $data) {
          
          $this->db->where('id', $id);
          $this->db->update('tb_event', $data);
      }
      
      function deleteEvent($id) {
          
          $this->db->where('id', $id);
          $this->db->delete('tb_event');
      }
            
      function upcomingEvent() {
          
          $sql = "SELECT * from tb_event where STR_TO_DATE(date_1, '%d/%m/%Y') > NOW()" ;
          return $this->db->query($sql)->result_array();
      } 
      
      function editPackage($id, $data) {
          $this->db->where('id', $id);
          $this->db->update('tb_package', $data);
      }
      
      function deleteOption($id) {
          
          $this->db->where('id', $id);
          $this->db->delete('tb_option');
      }
      
      function getUserList() {
          
          $query = $this->db->get_where('tb_user', array('user_type' => 111));
          return $query->result_array();
      }
      
      function getCoachList() {
          
          $query = $this->db->get_where('tb_user', array('user_type' => 222));
          return $query->result_array();
      }
      
      function deleteUser($id) {
          
          $this->db->where('id', $id);
          $this->db->delete('tb_user');
      }
      
      function deletePackage($id) {
          
          $this->db->where('id', $id);
          $this->db->delete('tb_package');
          
          $this->db->where('package_id', $id);
          $this->db->delete('tb_option');
      }
      
      function deleteCourse($id) {
          
          $this->db->where('id', $id);
          $this->db->delete('tb_course');
          
          $this->db->where('course_id', $id);
          $this->db->delete('tb_course_file');
 
      }
      
      function addUserToCoach($coach_id, $user_id) {
          
          $query = $this->db->get_where('tb_coach_user', array('coach_id' => $coach_id,
                                                    'user_id' => $user_id));
                                                    
          if ($query->num_rows() == 0) {
              $this->db->set('coach_id', $coach_id);
              $this->db->set('user_id', $user_id);
              $this->db->insert('tb_coach_user');
              return $this->db->insert_id();
          }          
      }
      
      function getUserListInCoach($coach_id) {
          
          return $this->db->select('A.*')
                       ->from('tb_coach_user as B')
                       ->join('tb_user as A', 'A.id = B.user_id')
                       ->where('B.coach_id', $coach_id)
                       ->get()
                       ->result_array();
      }
      
      function selectPackageOption($data) {
          
          $query = $this->db->get_where('tb_check_option', $data);
          
          if ($query->num_rows() > 0) {
              return false;
          } else {
              $this->db->insert('tb_check_option', $data);
              return true;
          }          
      }
      
      function editCourse($id, $data) {
          $this->db->where('id', $id);
          $this->db->update('tb_course', $data);
          
      }
      
      function deleteCourseFile($data) {
          
          $this->db->delete('tb_course_file', $data);
      }
      
      function getCourseForUser($user_id) {
          
          $result = array();          
                    
          $course_list = $this->db->select('tb_course.*')
                                  ->from('tb_course')
                                  ->join('tb_coach_user', 'tb_coach_user.coach_id = tb_course.user_id')
                                  ->where('tb_coach_user.user_id', $user_id)
                                  ->get()
                                  ->result_array();
                    
          foreach ($course_list as $course) {
              $files_query = $this->db->get_where('tb_course_file', 
                                                  array('course_id' => $course['id']));
              $files = array();
              if ($files_query->num_rows() > 0) {
                  
                  foreach ($files_query->result_array() as $row)
                  array_push($files, $row['file_url']);                  
              } 
              
              $course['file_url'] = $files;
              array_push($result, $course);
          }
          
          return $result;          
      }       
  }
?>
