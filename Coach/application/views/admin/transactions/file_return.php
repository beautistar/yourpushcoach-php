<link rel="stylesheet" href="<?= base_url() ?>public/custom_css/upload_button.css">
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-body with-border">
        <div class="col-md-6">
          <h4><i class="fa fa-reply"></i> &nbsp; File Return Portal</h4>
        </div>
        <div class="col-md-6 text-right">
          <a href="<?= base_url('admin/transactions'); ?>" class="btn btn-success"><i class="fa fa-list"></i> Transactions List</a>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box border-top-solid">
        <!-- /.box-header -->
        <!-- form start -->
        <div class="box-body my-form-body">
          <?php if(isset($msg) || validation_errors() !== ''): ?>
              <div class="alert alert-warning alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <h4><i class="icon fa fa-warning"></i> Alert!</h4>
                  <?= validation_errors();?>
                  <?= isset($msg)? $msg: ''; ?>
              </div>
            <?php endif; ?>
           
            <?php echo form_open_multipart(base_url('admin/transactions/file_return/'.$photo_id), 'class="form-horizontal"');  ?> 
              
              <div class="form-group">
                <label for="email" class="col-sm-2 control-label">Email</label>

                <div class="col-sm-9">
                  <input type="text" name="email" class="form-control" id="email" placeholder="">
                </div>
              </div>
              <div class="form-group">
                <label for="transaction_id" class="col-sm-2 control-label">Transaction ID</label>

                <div class="col-sm-9">
                  <input type="text" name="transaction_id" class="form-control" id="transaction_id" placeholder="">
                </div>
              </div>
              
              <div class="form-group">
                <label for="editor_id" class="col-sm-2 control-label">Editor ID</label>

                <div class="col-sm-9">
                  <input type="text" name="editor_id" class="form-control" id="editor_id" placeholder="">
                </div>
              </div>

              <div class="form-group">
                <label for="comment" class="col-sm-2 control-label">Comment Here</label>

                <div class="col-sm-9">                  
                  <textarea name="comment" id="comment" cols="50" rows="5" class="form-control"></textarea>
                </div>
              </div>
              <div class="form-group">
                <label for="file" class="col-sm-2 control-label">Browse</label>
                <div class="col-sm-9" align="bottom">                     
                  <input type="file" name="file" id="file" >
                </div>
              </div>              
  
              <div class="form-group">
                <div class="col-md-11">
                  <input type="submit" name="submit" value="Submit" class="btn btn-info pull-right">
                </div>
              </div>
            <?php echo form_close( ); ?>
          </div>
          <!-- /.box-body -->
      </div>
    </div>
  </div>  

</section> 